from lxml.html import fromstring
from BeautifulSoup import UnicodeDammit
from webstore.client import Database
import urllib
import difflib
from operator import itemgetter
import lambe_config

def decode_html(html_string):
    converted = UnicodeDammit(html_string, isHTML=True)
    if not converted.unicode:
        raise UnicodeDecodeError("Failed to detect encoding, tried [%s]",', '.join(converted.triedEncodings))
    return converted.unicode
    
def busca(empresa):
    base_url = "http://www.jucesponline.sp.gov.br/ResultadoBusca.aspx?"
    url = base_url + urllib.urlencode({"ppe" : empresa})
    print 'Getting ' + url
    html = urllib.urlopen(url).read()
    html = fromstring(decode_html(html))
        
    tabela = html.cssselect("th[abbr=NIRE]")[0].getparent().getparent()
    resultados = []
    for row in tabela:
        if row.cssselect("th"):
            pass
        else:
            data = {}
            data['nire'] = row.cssselect("td.item01 a")[0].text
            data['empresa'] = row.cssselect("td.item02 span")[0].text
            data['municipio'] = row.cssselect("td.item03")[0].text
            data['match_index'] = difflib.SequenceMatcher(None, data['empresa'].strip(), empresa).ratio()
            data['empresa_original'] = empresa
            data['best_match'] = 0
            resultados.append(data)
    resultados = sorted(resultados, key=itemgetter("match_index"), reverse=True)
    resultados[0]['best_match'] = 1
    table.writerows(resultados, unique_columns=['nire'])


def getDetalhes(nire):
    base_url = "http://www.jucesponline.sp.gov.br/Pre_Visualiza.aspx?nire="
    url = base_url + str(nire)
    print 'Getting ' + url
    html = urllib.urlopen(url).read()
    html = fromstring(decode_html(html))
    data = {}
    data['nire'] = nire
    data['tipo_sociedade'] = html.cssselect("#ctl00_cphContent_frmPreVisualiza_lblDetalhes")[0].text
    data['data_constituicao'] = html.cssselect("#ctl00_cphContent_frmPreVisualiza_lblConstituicao")[0].text
    data['data_inicio_atividades'] = html.cssselect("#ctl00_cphContent_frmPreVisualiza_lblAtividade")[0].text
    data['cnpj'] = html.cssselect("#ctl00_cphContent_frmPreVisualiza_lblCnpj")[0].text
    data['inscricao_estadual'] = html.cssselect("#ctl00_cphContent_frmPreVisualiza_lblInscricao")[0].text
    data['objeto'] = html.cssselect("#ctl00_cphContent_frmPreVisualiza_lblObjeto")[0].text_content().encode("iso-8859-1")
    data['capital'] = html.cssselect("#ctl00_cphContent_frmPreVisualiza_lblCapital")[0].text
    data['endereco_logradouro'] = html.cssselect("#ctl00_cphContent_frmPreVisualiza_lblLogradouro")[0].text
    data['endereco_numero'] = html.cssselect("#ctl00_cphContent_frmPreVisualiza_lblNumero")[0].text
    data['endereco_bairro'] = html.cssselect("#ctl00_cphContent_frmPreVisualiza_lblBairro")[0].text
    data['endereco_complemento'] = html.cssselect("#ctl00_cphContent_frmPreVisualiza_lblCep")[0].text
    data['endereco_cep'] = html.cssselect("#ctl00_cphContent_frmPreVisualiza_lblComplemento")[0].text
    data['municipio'] = html.cssselect("#ctl00_cphContent_frmPreVisualiza_lblCep")[0].text
    data['uf'] = html.cssselect("#ctl00_cphContent_frmPreVisualiza_lblUf")[0].text
    print 'Saving to thedatahub'
    table.writerow(data, unique_columns=['nire'])

def setup_db(user, apikey):
    database = Database('webstore.thedatahub.org', user, 'lambe', http_apikey=apikey)
    table = database['jucesp']
    return table

table = setup_db(apikey)    

